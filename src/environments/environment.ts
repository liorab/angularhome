// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBSDdtcnZfuKw-ug5M2GI0Wga-jKM99COk',
    authDomain: 'angularhome-ee369.firebaseapp.com',
    databaseURL: 'https://angularhome-ee369.firebaseio.com',
    projectId: 'angularhome-ee369',
    storageBucket: 'angularhome-ee369.appspot.com',
    messagingSenderId: '479272096400'
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
