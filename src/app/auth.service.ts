import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth,
    private router:Router) { 
      this.user=this.afAuth.authState;
    }

  signup(email:string, password:string){
    this.afAuth.auth.createUserWithEmailAndPassword(email,password).then(()=>{
      this.router.navigate(['/books']);
    })
  }

  login(email:string, password:string){
    this.afAuth.auth
    .signInWithEmailAndPassword(email,password)
    .then(()=>{
      this.router.navigate(['/books']);
    })
  }
  Logout(){
    this.afAuth.auth.signOut().then(res=> console.log('successful logout'));
  }
}
