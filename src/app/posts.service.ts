import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Posts } from './interfaces/posts';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiurl="https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient,private db:AngularFirestore) { }
  userCollection:AngularFirestoreCollection=this.db.collection('users');
 // postCollection:AngularFirestoreCollection=this.db.collection('posts');
 postCollection:AngularFirestoreCollection;
 

  getPosts(userId:string):Observable<any[]>{ 
    this.postCollection= this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(
      //pipe מאפשר לשרשר פונקציות
      map(
        //map גם מאפשר לשרשר פונקציות
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id= document.payload.doc.id;
            console.log(data);
            return data;
          }
        )
      )
    )
  }
  getPost(userId:string, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get();
  }
  savePosts(title:string, body:string, author:string){
    const post={title:title, body:body, author:author};
    this.postCollection.add(post);
  }
  addPost(userId:string,title:string, body:string, author:string){
    const post={title:title, body:body, author:author};
    this.userCollection.doc(userId).collection('posts').add(post);
  }
  updatePost(userId:string,id:string,title:string, body:string, author:string){
    const post = {title:title,body:body,author:author};
    this.db.doc(`users/${userId}/posts/${id}`).update(post);
  }
  deletePost(id:string,userId:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }
  
}
