import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  authors:any[] = [{id:1,name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'},{id:3,name:'Thomas Mann'}]
  i:number=3;

  getAuthors(){
    const authorsObservable = new Observable(
      observer => {
        setInterval(
          ()=>observer.next(this.authors),4000
        )
      }
    )
    return authorsObservable;
  }
  addAuthor(newauthor:string){
    this.i++;
    this.authors.push({id:this.i,name:newauthor});  
  }

  constructor() { }
}
