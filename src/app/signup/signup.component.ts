import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService:AuthService,private router:Router) { }

  email:string;
  password:string;

  ngOnInit() {
  }
  onSubmit(){
    this.authService.signup(this.email,this.password);
  }

}
