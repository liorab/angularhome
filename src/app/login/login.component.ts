import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authService:AuthService,private router:Router) { }

  email:string;
  password:string;

  ngOnInit() {
  }
  onSubmit(){
    this.authService.login(this.email,this.password);
  }

}
