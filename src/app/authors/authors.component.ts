import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})

export class AuthorsComponent implements OnInit {

  //authors:object[] = [{id:1,name:'Lewis Carrol'},{id:2,name:'Leo Tolstoy'},{id:3,name:'Thomas Mann'}]
  authors$:Observable<any>;
  

  constructor(private route: ActivatedRoute, private authorsservise:AuthorsService) { }


  name; 
  id;
  newauthor:string;

  ngOnInit() {
   this.name=this.route.snapshot.params.name;
   this.id=this.route.snapshot.params.id;
   // this.authorsservise.addAuthors(this.newauthor);
   this.authors$=this.authorsservise.getAuthors();
  }

  onSubmit(){
  this.authorsservise.addAuthor(this.newauthor);
  }
}
